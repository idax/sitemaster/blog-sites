---
latinum_slug: "terminal-split"
title: "Terminal Split"
thumbnail: blog/2020/terminal-split/thumbnail.png
lastmod: 2020-11-17T00:00:00
pinned: true
blog_categories: ["archdesign"]
summary: "Describes the different options that are available to developers regarding multi-pane terminal windows for better productivity."
---

# Introduction

When it comes to a discussion on terminals (or [command line interfaces](https://en.wikipedia.org/wiki/Command-line_interface)), you are deep in developer-land. So, before you proceed any further, if you are not aware of terminal applications, this would be probably be a good place to drop off.

In the world of text-based terminals and experiences with command line interfaces, a multi-pane terminal is very much like a multi-tabbed browser. Once you experience it, there is no going back ever again. To give you some idea, here are a few screenshots of the various multi-pane terminals that are popular with developers.

<div class="row">
	<div class="col-md-6">
		<img src="blog/2020/terminal-split/tilix-screenshot.png"/>
	</div>
	<div class="col-md-6">
		<img src="blog/2020/terminal-split/iterm-screenshot.png"/>
	</div>
</div>
<div class="row">
	<div class="col-md-6" style="font-weight:bold; text-align:center;padding:1em">
		Tilix on Ubuntu Budgie Desktop
	</div>
	<div class="col-md-6" style="font-weight:bold; text-align:center;padding:1em">
		iTerm2 on MacOS
	</div>
</div>

Each sub-window behaves like an embedded terminal, running its own shell (the interactive command line program that comes prebundled with the underlying operating system). The benefits are obvious: there are no overlapping windows to manage, you get a bird's eye view across all terminals and navigation between terminals is only a mouse click or key press away.

# Native Multi-Pane Terminals

These are terminal applications where the nested window behavior is owned by the application itself. The terminal implements the visual aspects of this behavior using split-pane features provided by the underlying windowing system. For each nested window, the terminal is furthermore responsible for starting a native shell (such as bash or powershell) in the background and attach its input and output to the corresponding window.

Here are details on some of the terminal applications that natively supports muli-pane behavior:

## Tilix

[Tilix](https://gnunn1.github.io/tilix-web/) is an advanced GTK3 tiling terminal emulator that follows the Gnome Human Interface Guidelines. It is currently available only on the Linux operating system and comes pre-bundled with some of the Ubuntu-based distros. Out of the box, Tilix would look and behave like any other terminal program on Linux. However, this experience can quickly scale upto a multi-tab interface, with each tab having multiple nested windows, each running a separate shell.

Aesthecially, Tilix is way ahead of the usual lot of terminal applications for Linux (e.g. rxvt and xterm). You can customize fonts, color schemes, window size and background transparency to achieve a broad spectrum of visual appeal. It has also got integration with file manager applications especially in distros where it comes pre-bundled.

## iTerm

[iTerm](https://iterm2.com/), and now iTerm2, started out much earlier than Tilix, as a replacement for the default terminal application that comes bundled with MacOS. It is currently available exclusively on MacOS, which seems to be its primary drawback. In fact, much of the development and features of Tilix were driven by the need to have an equivalent system for Linux-based systems. In terms of features and usability both iTerm2 and Tilix are pretty much equivalent of each other.

## Hyper

[Hyper](https://hyper.is/) is a relatively newer entry in the world of multi-pane terminals. It is built on top of [Electron](https://www.electronjs.org/) and makes use of open Web standards and toolkits such as React. One fallout of this approach is that it is relatively slower and less customizable than the other options described above. But these aspects are expected to improve as the product matures. On the upside, Hyper is available on Windows, Linux and MacOS. It is also available as a portable solution on Linux.

Here is a screenshot showing Hyper in action:

![hyper](blog/2020/terminal-split/hyper-screenshot.png)

# Emulated Multi-Pane Terminals

Also called terminal multiplexers, these are applications that run within a terminal and use a text-based interface to mimic the nested window behavior of Tilix and similar applications. Barring few exceptions, these applications will run in nearly all terminals, and are usually available for Windows, Linux and MacOS. Another good thing about multiplexers is that they can keep running in the background, even when the terminal application is closed. Later on, you can open a separate terminal window, connect to a background session, get back all your nested windows and resume from where you left off.

## TMux - The one and only

While it is not the only terminal multiplexer out there (alternatives being [dvtm](https://linux.die.net/man/1/dvtm) and [mtm](https://github.com/deadpixi/mtm)), TMux is probably the most popular and customizable of them all. With a little bit of effort you can have a user experience that is nearly the same as Tilix or iTerm. Here is a screenshot to drive home the point:

![TMux](blog/2020/terminal-split/tmux-screenshot.png)

The above image shows TMux running inside Tilix, which itself is running as a single window. The nested windows you see, are being supported by TMux.

# Which Terminal to Use?

Based on availability, I would recommend to keep handy a combination of either Tilix or iTerm and TMux on your desktop. This will allow you to fire up the best possible option for a given usage scenario. Here are some examples:

1. You meed to quickly fire up a terminal, either from the start menu or the file manager context menu, to perform a few comamnd line operations and then close the window. You are probably better off with a simple terminal window without any tiling support.

2. You need to open a terminal to perform more than one non-overlapping operation. Each operation requires its own environment and setup. For example, you might can be running repeatedly, and in quick succession, a software build operation, local debugging and test execution, and a source code checkin. You have a choice between using TMux and Tilix or iTerm.

3. You need to perform some of the above operations, with the same cadence, but on a remote machine over SSH. One option is to open multiple remote sessions in separate nested windows within iTerm or Tilix. A better alternative is to open a single remote session and then run TMux on the remote machine, and each of these operations in a nested window therein. An added advantage here is that you can always close the remote session and resume the same later from where you left off. This is possible since TMux stays active on the remote machine and keeps all your setup and running applications intact, until it is explicitly terminated.

4. The above scenario also holds good in a local setup when you wish to keep certain applications running for a long duration, but do not want to keep that many terminals open.

And lastly, if you find the above screenshot of TMux visually appealing, here is [the article](/potteringeek/#!/custom-tmux-setup) that describes how to configure TMux for the above experience and a few other usability tweaks.