---
latinum_slug: "common-mistakes-in-ddd"
title: "Common Mistakes in Domain-Driven Design"
thumbnail: blog/2021/common-mistakes-in-ddd/thumbnail.png
lastmod: 2021-02-01T00:00:00
pinned: true
blog_categories: ["archdesign"]
summary: "Underlining some of the common mistakes and misconceptions with Domain Driven Design."
---

> __Disclaimer:__ This article is not a course on domain-driven design (DDD in short) principles, patterns or practices. The reader is expected to have a preliminary exposure to DDD methodologies before proceeding to the rest of this document.

# Introduction

Domain-driven design is most effective at the cusp of product and engineering, by helping to create a mindshare between these two teams. Engineers can use these design concepts and models to portray the capabilties of the software systems they own, without going into the technicalities of development and deployment. This is more of an inside-out view which can then be validated and enriched further with inputs from product owners.

I have had the opportunity of leveraging domain-driven design while working with organizations in quite unrelated domains: large-scale ecommerce and finance management. One common experience that I have encountered over and over again, is that while senior developers and architects understand the theoretical aspects of DDD, they are not equally effective to benefit from the same in their design activities. This results in a sub-optimal utilization of DDD, and a marked lack of clarity and comptrehensibility of the design output.

In this post I will highlight certain misconceptions leading to sub-optimal use of DDD, based on my observations while working with diverse engineering teams.

# Database Schema = Domain Model

A common mistake made by many developers is to use database schemas and domain models interchangeably. A related assumption being made here is that the underlying storage is relational in nature. So what about strorage strategies that are columnar in nature, or are simple maps of key-value pairs? Here is an example to highlight that database schema and domain model are, in fact, very distinct concepts.

Consider a blogging platform that allows for blog posts to be grouped into categories. A given post can be associated with multiple categories at a time, resulting in a many-to-many relationship between posts and categories. Furthermore, a blog post can be associated with multiple comments in a one-to-many relationship.

Assuming that the underlying storage strategy is relational in nature, here is an entity-relationship diagram for the corresponding schema:

<div style="text-align:center">
<img src="blog/2021/common-mistakes-in-ddd/diagram01.png" style="max-width:750px">
</div>

While there are significant correspondence between tables in the database and domain entities, there are also differences in how these entities relate to each other in defining the state of the bounded context. This is as illustrated in the diagram below:

<div style="text-align:center">
<img src="blog/2021/common-mistakes-in-ddd/diagram02.png" style="max-width:700px">
</div>

Expand this section


# Domain modeling is only about durable/stateful elements

Yet another misconception shared by many developers is that the representation of a bounded context (in other words, a domain) is adequately represented by its internal state only. This thought can be traced back to the above mistake of treating domain model as an alternative to database schema.

The end result is that entities and value objects end up as being the only elements captured in a domain-driven design. The behavior around this state also demands equal representation, but is usually missed out.

A bounded context should include, at minimum, the following additional stereotypes in order to paint the complete picture for a given domain:

* __Repositories:__ responsible for storing and retrieving entities and value objects to and from storage.

* __Services:__ encapsulate pure functionality and does not have any associated state. The functionality usually spans across multiple domain entities that do not have a direct relationship with each other.


# Entity and value objects are data objects

By definition, a data object is a collection of attributes that form the input to or output from a given CRUD (create-retrieve-update-delete) operation. The only behavior associated with such objects are accessors (getters and setters) to access the encapsulated data.

Desigining entities to be pure data objects is the first step towards creating [anaemic domain models](https://en.wikipedia.org/wiki/Anemic_domain_model). This design style, while pervasive, is considered by most experts to be an anti-pattern.

What could be the additional behavior that can be associated with an entity? For the blogging platform example introduced earlier, a blog post entity can encapsulate the following behavior beyond basic data access:

* associate a category with the post (this behavior could also be replicated on the category entity for flexibility).
* add or remove comments for the blog post.
* alternate content generation, e.g.
  * extraction of a post summary for display on a list of posts
  * if the body format is markdown, generate and keep an HTML version ready for rendering on a Web page.
* content validation, e.g. check for spelling, grammar, broken links, association with related content, etc.

Some of these behavior, especially the ones pertaining to related and nested entities, are (incorrectly) delegated to repositories, and deemed implicit. Others, such as conversion from markdown to HTML, are (unnecessarily) moved into service elements that depends on the entity for the underlying content.

The corresponding domain model is as provided below:

<div style="text-align:center">
<img src="blog/2021/common-mistakes-in-ddd/diagram03.png" style="max-width:800px">
</div>

In the next section, we shall recommend a better model that targets at better ownership of specific behavior within the bounded context.

# CRUD operations are exclusive to repositories

The representation of repositories are usually excluded from a domain model for the following two reasons:

* every entity is assumed to have its own repository element (1:1 correspondence)
* the relationship between an entity and its repository always follow a common CRUD-style pattern (create-retrieve-update-delete).

This makes the repository implicit with CRUD as the only behavior.

Part of the problem stems from modeling entities as pure data objects, as explained in the previous section. Using the same example, a better alignment of behavior would be as shown below:

<div style="text-align:center">
<img src="blog/2021/common-mistakes-in-ddd/diagram04.png" style="max-width:850px">
</div>

To summarize,

1. A repository should serve as the initial context to create or delete an entity. Later, the same repository is used to retrieve one or more entities based on a query criteria.

2. Once created, the entity is responsible for updating any change in its state with the underlying repository. In this regard, the entity is in control of how the persistence is performed. For example, an explicit save operation may be provided that allows for multiple changes to the entity state before it is persisted. Alternatively, any single change to the entity (usually at an attribute level) is immediately persisted as part of the same operation.

3. For a 1:N relationship between a parent and a child entity, the parent has to own the following behavior:
    * create a new child entity and associate the same with self.
    * retrieve one or more child entites based on a query criteria.
    * remove a child entity.

4. Any updates to a child entity should follow the same approach as per #2 above. The containing entity should not be directly involved, unless it is modeled as an aggregate.

5. In case of M:N relationship between two top-level entities, either or both of the entities has to own the following behavior:
    * create an association with the other entity
    * delete any existing association with the other entity
    * retrieve all instances of the other entity with which it is associated, based on a query criteia.

Creation and deletion of each entity should follow the same approach as described in #1 above.

# 5. Purely functional domains do not require modeling

Expand this section


# 6. No room for aggregates

A complete lack of aggregates in domain models, like a complete lack of behavioral elements, is a sign that the underlying design principles can be refined further. In fact, given enough functional complexity, most bounded context will merit at least one aggregate as the root to traverse to nearly every other entity and value objects within its scope.

Perceived simplicity triumphs design elegance. It is easier to think of entities as being normalized, instead of being nested. It is also easier to think of transaction boundaries as encompassing a single entity at a time. Any consistency across entities and sub-entities are usually enforced synthetically from within a repository or a service.

In our example blogging platform, we can think of the blog post, with associated comments, as an aggregate. The corresponding domain model would then look like as follows:

[diagram here]

Note the single `save` operation with the blog post entity. This is a single operation to persist all previous changes on the post and associated comment entities. In the process, it encapsulates any transaction capabilities (e.g. commit and rollback) with the underlying storage system.

# References and Further Reading

1. [Domain Driven Design Crash Course](https://vaadin.com/learn/tutorials/ddd): a series of articles, where you learn what domain-driven design is and how to apply it - or parts of it - to your projects.
