---
latinum_slug: "poor-man-repository"
title : "Assembling a Poor Man's Maven Repository"
blog_categories: ["devenv"]
summary: Recipe to setup a remote Maven repository using nothing more fancy than a Nginx Web Server and some storage space.
thumbnail: blog/2020/poor-man-repo/thumbnail.png
lastmod: 2020-12-30T00:00:00
pinned: true
---

# Introduction

A remote Maven repository provides a environment to publish and share Java class libraries against defined release versions. Apart from Maven, It can also be used other build management tools such as Gradle and Ivy, to resolve project dependencies and retrieve the same as binary artifacts.

Publicly available Maven repositories, such as [Maven Central](http://repo.maven.apache.org), [Sonatype](http://central.sonatype.org) and [Bintray](http://www.bintray.com) can be quite restrictive when it comes to deploying your artifacts for sharing with the entire developer community. You may also want to have a private repository that is available only to a closed group or to an organization. In such cases you may consider setting up a remote repository within an environment that you own.

Several free repository managers exists today for remote repository management, Apache Nexus and JFrog Artifactory being popular examples. Being feature-rich, these managers have a non-trivial hardware and software requirements, setup and maintenance.

In this post, we will describe a recipe to setup a remote Maven repository using nothing more fancy than a Nginx Web Server and some file system storage space on the server.

# What You Get (and What You Don't)

This being a poor man's repository, expect nothing more than the base essentials of publishing and retrieving binary artifacts against a version scheme with minimal access control. Specifically,

1. You can push the binary output of a Maven build process to the repository using HTTP protocol.
2. You can control who can get to publish to the repository using a set of user names and passwords, one for each user.
3. You can retrieve the binary artifacts from the repository during a build operation on a development environment using Maven, Gradle or Ivy as the build manager. The underlying protocol is HTTP and there is no access control to specific artifacts or their versions.
4. You can remotely browse the contents of the repository, using a Web server without any restrictions.

Some of the most obvious capabilities of a repository manager that you will miss are:

1. Fancy browsing of the repository including meta data browsing, visual dependency graphs and search.
2. Proxy of remote repositories and combine the contents of multiple repositories into a single virtual repository.
3. Detailed access control, user profile management with pluggable authentication such as using LDAP.
4. Administration console to ease management and maintenance of repositories.

Of course, depending on where it is deployed (Internet zone), you can still use the poor man's repository to host your artifacts for the whole world to consume, or keep it local to your group or organization.

# Let's Assemble

## Assumptions and Prerequisites

For the purpose of this recipe, we will assume that the operating system to be Ubuntu Linux 17.10 or greater. However, you should be able to repeat the steps for a Windows or a Mac system with practically no variations.

## Step 1: Install Nginx with WebDAV support

WebDAV is the protocol that is used by Maven wagon plugin to push the output of a build process to a remote repository. Depending on the Ubuntu version, the default package for `nginx`, or even `nginx-extras` package may not come with the additional WebDAV support modules. You need to install the full package of nginx for WebDAV to work.

```
$ sudo apt update
$ sudo apt install nginx-full
```

Post installation nginx should be up and running. Use a Web browser to connect with the machine on 80 to get the default nginx homepage and verify this step.

## Step 2: Setting up Authentication for Write Access

Our mechanism to achieve this would be a simple user name and password based scheme using HTTP basic authentication. The user name and password pairs are stored in an external file from where they are read by nginx.

Check if `openssl` is installed on the repository machine

```
$ which openssl
/usr/bin/openssl
```

The package is not installed if you do not get the second line as response, in which case you will have to install it.

```
$ sudo apt install openssl
```

Let us assume that you wish to create a set of credentials using the user name **poorman** and password **biteme**. Let this credential be stored in the file `/opt/maven-repo/credentials`. Enter the following command at the shell prompt:

```
$ printf "poorman:$(openssl passwd -apr1)\n" >> /opt/maven-repo/credentials
```

You will be asked to enter a password, to which you type **biteme** and retype the same to verify. Check the contents of the file `/opt/maven-repo/credentials`, you should see a line that looks like this:

`poorman:$apr1$/4PpODPG$sKAH1vIec7SNAApDbPa1V/`

Note: file paths starting from `/opt` usually do not have write privileges for a regular user. You may need to prepend the shell command with a sudo.

## Step 3: Setting up WebDAV

We will be setting up a snapshot repository and a release repository whose contents are hosted on the file system under the paths `/opt/maven-repo/files/snapshots` and `/opt/maven-repo/files/releases` respectively. The nginx configuration for WebDav access to these folders is as follows:

```nginx
location /deploy/ {
  alias                  /opt/maven-repo/files;
  dav_methods            PUT;
  client_body_temp_path  /var/tmp;
  create_full_put_path   on;
  dav_access             user:rw group:rw all:r;
  auth_basic             "poor man's maven repository";
  auth_basic_user_file   /opt/maven-repo/credentials;
}
```

This will make the maven snapshot repository available for binary uploads at `http://yourhostip/deploy/snapshots`. The corresponding release repository is available at `http://yourhostip/deploy/releases`.

## Step 4: Setting up Repository Browsing

This would be the basic directory listing and browsing interface provided by nginx on folders mapped to aliases with autoindex option enabled. The corresponding nginx configuration is as follows:

```nginx
location /view/ {
  alias /opt/maven-repo/files;
  autoindex on;
}
```

Putting it together, the complete nginx configuration would look something like this:

```nginx
server {
  listen 80;
  listen [::]:80;

  server_name _;

  location /view/ {
    alias /opt/maven-repo/files;
    autoindex on;
  }

  location /deploy/ {
    alias                  /opt/maven-repo/files;
    dav_methods            PUT;
    client_body_temp_path  /var/tmp;
    create_full_put_path   on;
    dav_access             user:rw group:rw all:r;
    auth_basic             "maven repo restricted";
    auth_basic_user_file   /opt/maven-repo/credentials;
  }
}
```

Your repository is now ready for remote use and access.

# Using with Maven Build

## Download from the Repository

In order to make use of the new repository to download artifacts and resolve dependencies, you need to make the following changes to your Maven build file:

```xml
<repositories>
  <!-- repository location for xyz binaries -->
  <repository>
    <id>pm-release</id>
    <url>http://yourhostip/view/releases</url>
    <snapshots>
      <enabled>false</enabled>
    </snapshots>
  </repository>

  <repository>
    <id>pm-snapshot</id>
    <url>http://yourhostip/view/snapshots</url>
    <snapshots>
      <enabled>true</enabled>
    </snapshots>
  </repository>

  <!--
  The default Maven repository must also be defined to load additional
  binaries, if required by your project.
  -->
  <repository>
    <id>central</id>
    <name>Maven Repository Switchboard</name>
    <layout>default</layout>
    <url>http://repo1.maven.org/maven2</url>
    <snapshots>
      <enabled>false</enabled>
    </snapshots>
  </repository>
</repositories>
```

### Upload to the Repository

For deploying Java binaries to the new repository, as an outcome of `maven build install` you need to do the following:

Make the following changes to maven settings file, usually available at `~/.m2/settings.xml`:

```xml
<servers>
  <server>
    <id>pm-snapshot</id>
    <username>poorman</username>
    <password>biteme</password>
  </server>
  <server>
    <id>pm-release</id>
    <username>poorman</username>
    <password>biteme</password>
  </server>
</servers>
```

This sets up the user names and passwords to access remote repositories aliased using `pm-snapshot` and `pm-release`. Now make the following changes to your maven build file (`pom.xml`)

```xml
<distributionManagement>
  <repository>
    <id>pm-release</id>
    <url>http://yourhostip/deploy/releases</url>
  </repository>
  <snapshotRepository>
    <id>pm-snapshot</id>
    <url>http://yourhostip/deploy/snapshots</url>
  </snapshotRepository>
</distributionManagement>
```

## Endnotes

If you are developing on a budget, or in a small private team, the poor man's repository is an easy recipe to unblock for iterative and modular development. It's low hardware overheads and minimal software requirements allows it to be setup on legacy systems (an old functional desktop that is not being used) and even on portable and low-powered units, such as raspberry pi. You can also choose a public cloud-based hosting option (such as AWS and Linode) at a minimal cost for sharing your binaries with a larger developer community.
