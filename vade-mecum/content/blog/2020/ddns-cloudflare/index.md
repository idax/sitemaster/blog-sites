---
latinum_slug: "ddns-cloudflare"
title : "Dynamic DNS Hack using Cloudflare"
thumbnail: blog/2020/ddns-cloudflare/thumbnail.png
lastmod: 2020-12-19T00:00:00
pinned: true
summary: A step-by-step guide towards setting up dynamic DNS resolution using Cloudflare, to serve Internet traffic from within your home network.
---

# Dynamic DNS

Like most of you Internet Users, my home wifi has a public facing IP address that is not permanently assigned and tend to change over time. Like some of you, I like to have a small server-side environment setup within my home network, that should be accessible from anywhere on the Internet using a domain name (e.g. https://www.idax.me).

Domain names must be mapped to IP addresses, and your domain name provider should provide you with the necessary administration interface (typically web-based) to modify the same. The IP address should be static/fixed, else name resolution will fail. Unless you have a mechanism to update the DNS records with you provider everytime the IP address changes.

This is the concept of Dynamic DNS (DDNS, for short).

In this article, I will explain a quick hack that I put in place to enable DDNS using Cloudflare as the nameserver and Web gateway, and some simple scripting using Python.

# Available Alternatives

There are quite a few DDNS solutions that I had tried out, and rejected, before coming up with my hack. The criteria for evaluation in each case are as follows:

1. It should have a free subscription tier that is still usable.
2. Going forward, the premium features should still come at a reasonable price point.
3. It should allow me the flexibility of choosing my DNS provider.
4. There should be no hidden costs.

Here is a table that summarizes my findings for the alternatives that were explored:

| Criteria | [DynDNS](http://www.dyn.com) | [No-IP](https://www.noip.com/) | [Google Domains](https://domains.google.com) | [Amazon Route 53](https://aws.amazon.com/route53/) |
| -------- | ------ | ----- | -------------- | --------------- |
| Free Subscription Tier | No | Yes, upto 3 domains | No | No |
| Usable free features | No | Choice of domain names limited to subdomains against a predefined set of base domain names (e.g. *.ddns.net or *.hopto.org) and not to a domain name that you own | No | No |
| Choice of DNS providers | Yes | Yes. But usability of custom domain names is limited | No. Is a DNS provider in itself. Can do a one-time migration of a pre-owned domain name from an external provider | No. Is a DNS and web host solution combined |
| Pricing for Premium Features | pricing per year  | pricing per year for X domains  | pricing per domain per year | have a combination of one-time and pay-as-you-go pricing rules that may lead to unexpected costs |

# About Cloudflare

Cloudflare started off as a CDN (Content Delivery Network) that quickly expanded to include edge security solution, web acceleration and proxy. It is not a hosting provider but does include a DNS nameserver for traffic routing. This means that you can use Cloudflare as a DDNS provider if you can find the right interface to update the IP address when it changes for your home network.

One good thing about Cloudflare is that it provides a REST-based, modern API for remote configuration that includes DNS entries. On the flip side, the client applications for IP refresh do not include the latest binaries for all platforms, which means you will need to build the same from source. Some of the older applications (e.g. ddclient) are not well supported or maintained for Cloudflare.

# Setting up the Providers

You domain name is registered with the provider of your choice (I prefer [GoDaddy](https://in.godaddy.com)). You should have access to the corresponding interface to modify the domain name entries with your provider.

You should have an account with Cloudflare. A free account is sufficient for this hack to work.

Add a site on Cloudflare by providing the domain name that is avaliable with your DNS provider.

<div align="center">

![Create a New Site](blog/2020/ddns-cloudflare/duc-post-01.png)

</div>

Cloudflare will import the DNS records (A, AAA, CNAME, MX, etc.) from the DNS provider and keep everything ready for traffic routing.

<div align="center">

![Imported Domain Entries](blog/2020/ddns-cloudflare/duc-post-02.png)

</div>

As the last step, Cloudflare will require you to change the nameservers, from the ones that are assigned by your DNS provider, to the ones that are owned by Cloudflare:

```
dee.ns.cloudflare.com
rodney.ns.cloudflare.com
```
You need to login to your DNS provider's management interface and modify the nameserver entries to point to the ones listed above.

<div align="center">

![Godaddy's Nameserver Changes](blog/2020/ddns-cloudflare/duc-post-03.png)

</div>

You are all ready for dynamic IP updates to happen from your home network.

# Dynamic Update Client

The dynamic update client is a script that is deployed on one of the machines within the home network, to do the following:

1. Obtain the public IP address that is assigned to your home network by your ISP. This is done through a service call to [ipify](https://www.ipify.org/).
2. Use Cloudflare's REST APIs to update the __A record__ for the site that has been added during provider setup.
3. Run priodically as a cron job on the hosted machine to do steps #1 and #2 above.

The script is written in Python and requires version 3.x, and above, of the interpreter. It can run out-of-the-box on both Linux and MacOS. Running the same on Windows will require you to setup a Python environment separately. The rest of the instructions will assume deployment in a Linux environment.

The source code of the client is hosted on Gitlab as a [public repository](https://gitlab.com/idax/duc-cloudflare). There are two versions available, with virtually the same implementation but different invocation. One of them is [duc-cloudflare.py](https://gitlab.com/idax/duc-cloudflare/-/raw/master/duc-cloudflare.py) that requires a manual use of the python interpreter to run.

```bash
$ python duc-cloudflare.py
```
The same code is also available as a bash script called [duc-cloudflare](https://gitlab.com/idax/duc-cloudflare/-/raw/master/duc-cloudflare). You will still need python, but the commands to invoke the script will change to:

```bash
$ chmod +x duc-cloudflare
```
The above is an one-time operation to make the script executable, and then:

```bash
$ ./duc-cloudflare
```

In both cases, the output of the script will look something like this:

```
zone_id     : 455f75fd360c47a2aa6753a3e2008b9c
email       : you@your-domain.com
API Token   : 506bca2875074c8794a376f0416d9c83c33fc
domain name : your-domain.com
public IP address is: 121.170.96.124
A record found. Updating
DDNS update successful
```

## Configuring the Update Client

In order to run, the Dynamic Update Client needs access to certain information that are speccific to your site and your account with Cloudflare. This information is read from a file that must be named duc-cloudflare.ini and must reside in the same folder as the client script. The structure
and comntent of this file are as follows:

```ini
[cloudflare]
zone_id = cab2e7950f352f99e1b8aeb8e116e25b
email = youremail@domain.com
api_token = 2f4ee5fea1366fb4395c1c5ca01467a0c33fc

[dns]
domain_name = idax.me
```

The values against each configuration item are only for the purpose of illustration. You need to replace the same with information that are pertinent to your setup.

zone_id : Each domain that is being routed through Cloudflare, forms a different zone. The zone id is used to uniquely refer to each such zone. This information is available on the main page for the corresponding zone.

email: This is the email that you use as the username while logging into the Cloudflare Web portal.

api_token: This is an authorization token that is used to validate each API call being made to Cloudflare. You can use the global API key that is associated with your account and is available under "My Profile" section of the Web portal.

<div align="center">

![Global API Key](blog/2020/ddns-cloudflare/duc-post-04.png)

</div>

domain_name: This is the base domain name that you own. Note that the client will only update the A record of DNS. Sub-domains (e.g. blog.dax.me) are created as CNAME records, and must be separately configured and point towards the A record on Cloudflare.

# Run the Update Client as a Background Job

You can run the Dynamic Update Client manually from a terminal for the corresponding operating system. There is no provision to detect a change in the public IP address and update Cloudflare automatically. You can, however, run `duc-cloudflare` as a cron job on Linux as enumerated by the steps below.

A corresponding syntax may also exist for MacOS and Windows, but will not be addressed here.

__Step 1: Organize Your Files__

Move the `duc-cloudflare` and `duc-cloudflare.ini` to a folder that is accessible to the operating system without any restrictions. I recommend creating and using the folder `opt/duc-cloudflare` for this purpose.


__Step 2: Setup Permissions__

After you create the above folder for DUC, and move the required files, make sure the ownership of the folder and its contents is with the root account.

```bash
$ sudo chown -R root:root /opt/duc-cloudflare
```

Make sure that the file `duc-cloudflare` has the execute flag set.

```bash
$ sudo chmod +x /opt/duc-cloudflare/duc-cloudflare
```

__Step 3: Modify Cron tab__

To open the crontab configuration file for the root user, enter the following command in your terminal window:

```
$ sudo crontab –e
```

Then add the following line:

```
*/5 * * * *  /opt/duc-cloudflare/duc-cloudflare
```

This will cause the script to run every 5 minutes to check for a change in the public IP address, and update Cloudflare if necessary.

__Step 4: Restart Cron__

Restart the cron service and you are ready to go.

```bash
sudo service cron restart
```

# Endnotes

Once the DNS is updated with Cloudflare, it takes an additional lead time for the changes to propagate to other name servers worldwide. This time is usually upto 10 minutes, which, when added to the script itself running every 5 minutes, can create a 15 minutes window of inconsistency. During this time, any browse operation on your site can fail due to the old IP address being no longer valid.

Choosing a DDNS for your home network may not always be the most effective setup. You need to consider the following:

1. How often is the IP address of your site getting changed? Is it on every restart of your dialer/main router or every _x_ hours? The more frequent is the change, the less effective a DDNS setup is going to be. To partially mitigate this, you can configure the update client to run every minute, instead of 5.

2. How busy is your site? Do you encounter a continuous stream of traffic or is it highly intermittent? DDNS setup is usually not suited for busy sites with decent daily traffic, because there is always a window of time when the site is not available whenever there is a change in IP address. The only mitigation is to use a static IP address, provided by your ISP, for your home network.
