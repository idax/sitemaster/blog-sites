import http.client
import json
import configparser

# Read the configuration data from ddns-hack.ini and read
# in the following information
#

config = configparser.ConfigParser()
config.read("duc-cloudflare.ini")

zone_id = config.get("cloudflare", "zone_id")
email = config.get("cloudflare", "email")
api_token = config.get("cloudflare", "api_token")
domain_name = config.get("dns", "domain_name")

print ("zone_id     : " + zone_id)
print ("email       : " + email)
print ("API Token   : " + api_token)
print ("domain name : " + domain_name)

# Fetch the public IP address of the home network
con1 = http.client.HTTPSConnection("api.ipify.org")
con1.request("GET", "/")
resp1 = con1.getresponse()
my_ip = resp1.read().decode("utf-8")
con1.close()

print ("public IP address is: " + my_ip)

headers = {"Content-type" : "application/json", "X-Auth-Email" : email, "X-Auth-Key" : api_token }

con2 = http.client.HTTPSConnection("api.cloudflare.com")
con2.request("GET", "/client/v4/zones/" + zone_id + "/dns_records", None, headers)
resp2 = con2.getresponse()
zd = resp2.read()
zone_details = json.loads(zd)
con2.close()
record_id = None

for record in zone_details["result"]:
    if record["type"] == "A":
        record_id = record["id"]
        break

a_data = { "type" : "A", "name" : domain_name, "content" : my_ip, "ttl" : 1, "proxied" : False }

con3 = http.client.HTTPSConnection("api.cloudflare.com")
if record_id is None:
    print ("A record not found. Adding new")
    con3.request("POST", "/client/v4/zones/" + zone_id + "/dns_records", json.dumps(a_data), headers)
else:
    print ("A record found. Updating")
    con3.request("PUT", "/client/v4/zones/" + zone_id + "/dns_records/" + record_id, json.dumps(a_data), headers)

resp3 = con3.getresponse()
update_resp = json.loads(resp3.read())

if update_resp["success"] :
    print ("DDNS update successful")
else:
    print ("DDNS update failed")
