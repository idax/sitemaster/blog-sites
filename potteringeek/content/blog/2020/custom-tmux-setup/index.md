---
latinum_slug: "custom-tmux-setup"
title : "A Custom TMux Setup"
blog_categories: ["devenv"]
summary: Quickly setup tmux for development and configure the same for a pleasant and user friendly experience.
thumbnail: blog/2020/custom-tmux-setup/thumbnail.png
lastmod: 2020-11-07T00:00:00
pinned: true
comments_enabled: true
---

# What is TMux?

TMux, short for terminal multiplexer, allows you to split a character-based terminal into multiple logical sub-terminals, each of which can run its own shell program. It also allows you to leave these logical terminals, and come back to them later without interrupting the running process(es).

TMux is preferred over a terminal that natively supports splitting (such as tilix and terminator) when you are connected to a remote terminal over SSH or telnet. In such cases, a single connection can be used to access any number of sub-terminals (TMux panes). Also, the sub-terminals, and the
programs running therein, are not lost in case the SSH connection terminates abruptly due to network glitches.

# The Final Outcome

Once you have followed all the instructions in this article to setup a tmux environment, you should get a terminal that can look like this:

![provide image](blog/2020/custom-tmux-setup/tmux-post-01.png)

In addition, the following features are also enabled:

1. The shortcut keys to split the terminal, or a pane, horizontally and vertically are mapped to Ctrl-A followed by the | and - keys respectively. This should be more intuitive than the default bindings provided by TMux.

2. Resize of split panes using the mouse cursor. On terminals with mouse support (such as the ones that are running in a graphical desktop environment) you can click on a line dividing any two panes and then drag the mouse to resize those panes.

# Step-By-Step Instructions

## Install TMux

TMux is available in the public repositories of the most popular distros. The package manager is the recommended route to install TMux on any Linux environment.

For example, under Ubuntu you can use the following terminal command to install TMux:

```bash
sudo apt install tmux
```

For Mac you can install and use Homebrew as the package manager for Linux applications. The rest of this article assumes a Linux operating environment for the Tmux setup under consideration.

## The Configuration File

Nearly every aspect of the Tmux user experience is controlled by instructions contained in a configuration file. The configuration for the screenshot above is vailable from here. You can download the file, or create a new one locally, and then copy the following content:

```python
# A config file to customize tmux.
# ..............................................................................

# Reload tmux conf file, after making changes
bind r source-file ~/.tmux.conf \; display "Reloaded ~/.tmux.conf"

# remap prefix from 'C-b' to 'C-a'
unbind C-b
set-option -g prefix C-a
bind-key C-a send-prefix

# split panes using \ and -, make sure they open in the same path
bind \\ split-window -h -c "#{pane_current_path}"
bind - split-window -v -c "#{pane_current_path}"
unbind '"'
unbind %

# switch panes using Alt-arrow without prefix
bind -n M-Left select-pane -L
bind -n M-Right select-pane -R
bind -n M-Up select-pane -U
bind -n M-Down select-pane -D

# Enable mouse mode (tmux 2.1 and above)
set -g mouse on

# Allow 256 colours to be used in the terminal:
set -g default-terminal "screen-256color"

set -g history-limit 10000

# Usually, sessions, windows and panes are numbered starting from 0. The next
# two lines change this, so that the windows and panes are numbered starting
# from 1. There is currently no easy way to ensure that sessions are numbered
# starting from 1.
set -g base-index 1
set -g pane-base-index 1

# Setting escape time delay to be smaller to make tmux more responsive to
# commands
set -s escape-time 1

setw -g automatic-rename on

#-------------------------------------------------------------------------------
# Customizations for panes
#-------------------------------------------------------------------------------

# set inactive/active window styles
# set -g window-style 'fg=colour002,bg=colour234'
# set -g window-active-style 'fg=colour002,bg=colour234'
set -g window-style 'fg=colour002,bg=colour233'
set -g window-active-style 'fg=colour002,bg=colour233'

#pane border
set -g pane-border-style 'fg=colour154,bg=colour234'
set -g pane-active-border-style 'fg=colour154,bg=colour234'

#-------------------------------------------------------------------------------
# Customizations for status bar
#-------------------------------------------------------------------------------

# enable activity alerts
setw -g monitor-activity on
set -g visual-activity on

# Update the status bar every ten seconds (for the time)
set -g status-interval 10

# statusbar colours
set -g status-style 'fg=black,bg=colour154'

# Status bar left-hand side
set -g status-left-length 30
set -g status-left "#[fg=white,bg=colour124] sess: #S #[fg=white,bg=colour94] win: #I #[fg=black,bg=colour10] pane: #P "

set -g status-justify centre
setw -g window-status-format "  #W  "
setw -g window-status-current-format "  #W  "
setw -g window-status-style 'fg=white,bg=colour22'


#Time and date on right-hand side
#-------------------------------------------------------#
# Status line right side
# e.g. 28 Nov 18:15
set -g status-right "#[fg=black,bg=colour10] #H %a %d-%b-%G %I:%M %p "
```

## Setup The Configuration File

We recommend that the configuration file with the above content be stored under `~/tmux.conf` where the `~` expands out to the full path for the current user's home directory. In the user's home directory create a new file called `.tmux.conf`:

```bash
cd
touch .tmux.conf
```

And then modify `.tmux.conf` with the following content:

```bash
source ~/tmux.conf
```

## Running TMux

Once you are done with the above changes, kill any ongoing sessions with Tmux. This configuration assumes that you are using a terminal that supports a minimum of 256 colors. in order to force this understanding with tmux, you need to start Tmux with the following command line:

```bash
$ tmux -2
```
